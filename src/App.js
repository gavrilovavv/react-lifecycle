import './App.css';
import React from 'react';
import { Root } from './hw7/Root';

const App = () => {
	return (
		<Root />
	);
};

export default App;
